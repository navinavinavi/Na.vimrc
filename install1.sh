#!/bin/bash
sudo apt-get install zsh &&
    sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)" &&
    git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim &&
    vim +PluginInstall +qall
